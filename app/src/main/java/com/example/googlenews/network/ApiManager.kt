package com.example.googlenews.network

import com.example.googlenews.utils.Constants
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Dispatcher
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiManager {
    private var retrofit: Retrofit
    private val dispatcher: Dispatcher = Dispatcher()
    private val gson: Gson = GsonBuilder()
        .excludeFieldsWithModifiers(java.lang.reflect.Modifier.TRANSIENT)
        .create()

    private val loggingInterceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    private val headerInjection = Interceptor { chain ->
        return@Interceptor chain.proceed(
            chain.request()
                .newBuilder()
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .build()
        )
    }

    init {
        retrofit = Retrofit.Builder()
            .baseUrl(Constants.API_URL)
            .client(
                OkHttpClient.Builder()
                    .addInterceptor(loggingInterceptor)
                    .addInterceptor(headerInjection)
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
                    .dispatcher(dispatcher)
                    .readTimeout(10, TimeUnit.SECONDS)
                    .build()
            )
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
    }

    fun call(): APICalls = retrofit.create<APICalls>(APICalls::class.java)
}