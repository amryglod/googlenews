package com.example.googlenews.network

import retrofit2.HttpException

sealed class Results<out T : Any> {
    class Success<out T : Any>(val response: T) : Results<T>()
    class Failure(val httpException: HttpException) : Results<Nothing>()
    object NetworkError : Results<Nothing>()
}