package com.example.googlenews.network

import com.example.googlenews.models.NewsResponse
import com.example.googlenews.utils.Constants
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface APICalls {

    @GET("v2/top-headlines")
    fun getNews(
        @Query("sources") source: String = "google-news",
        @Query("apiKey") apiKey: String = Constants.API_KEY
    ): Deferred<NewsResponse>
}
