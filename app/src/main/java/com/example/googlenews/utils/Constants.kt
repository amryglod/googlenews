package com.example.googlenews.utils

object Constants {
    const val API_URL = "https://newsapi.org/"
    const val API_KEY = "86766432c3a549b59e178a68af338e88"

    const val SERVER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ"
    const val APP_DATE_FORMAT = "MMM d, yyyy"

    const val DB_NAME = "database.db"
}