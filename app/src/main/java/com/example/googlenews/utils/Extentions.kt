package com.example.googlenews.utils

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.navigation.Navigation
import com.example.googlenews.R
import com.example.googlenews.network.Results
import kotlinx.coroutines.Deferred
import retrofit2.HttpException
import java.text.SimpleDateFormat
import java.util.*

suspend fun <T : Any> Deferred<T>.go(): Results<T> {
    return try {
        Results.Success(this.await())
    } catch (e: HttpException) {
        Results.Failure(e)
    } catch (e: Throwable) {
        Results.NetworkError
    }
}

fun androidx.swiperefreshlayout.widget.SwipeRefreshLayout.setColor(colorId: Int = R.color.colorPrimary) {
    this.setColorSchemeColors(ContextCompat.getColor(context!!, colorId))
}

fun View?.navigate(resId: Int, bundle: Bundle) =
    Navigation.findNavController(this!!).navigate(resId, bundle)

fun View?.up() = Navigation.findNavController(this!!).navigateUp()

fun View.setVisible() {
    if (this.visibility != View.VISIBLE) this.visibility = View.VISIBLE
}

fun View.setGone() {
    if (this.visibility != View.GONE) this.visibility = View.GONE
}

fun View.setInvisible() {
    if (this.visibility != View.INVISIBLE) this.visibility = View.INVISIBLE
}

fun Context?.toast(message: String) = Toast.makeText(this, message, Toast.LENGTH_LONG).show()

fun ProgressBar.setColor(colorId: Int = R.color.colorPrimary) = this
    .indeterminateDrawable
    .setColorFilter(
        ContextCompat.getColor(this.context, colorId),
        android.graphics.PorterDuff.Mode.MULTIPLY
    )

fun String.changeDateFormat(
    inputFormat: String = Constants.SERVER_DATE_FORMAT,
    outputFormat: String = Constants.APP_DATE_FORMAT
): String {

    val inputFormDate = SimpleDateFormat(inputFormat, Locale.ENGLISH)
    val outputFormDate = SimpleDateFormat(outputFormat, Locale.ENGLISH)

    val date = inputFormDate.parse(this)
    date?.let {
        return outputFormDate.format(date)
    }
    return ""
}



