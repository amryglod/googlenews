package com.example.googlenews.core.base

object BaseContract {

    interface FragmentView {
        fun showInternetConnectionError()
        fun showError(error: String?)
    }

    interface FragmentPresenter<in V : FragmentView> {
        fun attachView(view: V)
        fun detachView()
    }
}