package com.example.googlenews.core.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.googlenews.R
import com.example.googlenews.core.base.BaseFragment
import com.example.googlenews.core.details.DetailsFragment
import com.example.googlenews.models.ArticlesItem
import com.example.googlenews.utils.navigate
import com.example.googlenews.utils.setColor
import com.example.googlenews.utils.setGone
import com.example.googlenews.utils.setVisible
import kotlinx.android.synthetic.main.fragment_news.*

class MainFragment : BaseFragment<MainContract.View, MainContract.Presenter>(),
    MainContract.View {
    private lateinit var adapter: MainAdapter
    override var presenter: MainContract.Presenter = MainPresenter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_news, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initRecyclerView()
        initListeners()
        initBuilder()

        presenter.onStartView()
    }

    override fun hideProgressBar() {
        swipeToRefresh.isRefreshing = false
        progressBar.setGone()
    }

    override fun showProgressBar() {
        progressBar.setVisible()
    }

    override fun setNews(list: List<ArticlesItem>?) {
        adapter.setData(list)
    }

    override fun showDetailsFragment(news: ArticlesItem) {
        view.navigate(R.id.goDetailsFrag, Bundle().apply {
            putSerializable(DetailsFragment.KEY_NEWS, news)
        })
    }

    private fun initListeners() {
        swipeToRefresh.setOnRefreshListener {
            presenter.onSwipeToRefresh()
        }
    }

    private fun initBuilder() {
        swipeToRefresh.setColor()
        progressBar.setColor()
    }

    private fun initRecyclerView() {
        adapter = MainAdapter(
            click = {
                presenter.onItemClick(it)
            }
        )
        newsRV.adapter = adapter
    }
}