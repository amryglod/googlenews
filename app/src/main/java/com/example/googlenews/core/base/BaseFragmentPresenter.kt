package com.example.googlenews.core.base

open class BaseFragmentPresenter<V : BaseContract.FragmentView> : BaseContract.FragmentPresenter<V> {
    protected var view: V? = null

    override fun attachView(view: V) {
        this.view = view
    }

    override fun detachView() {
        view = null
    }
}