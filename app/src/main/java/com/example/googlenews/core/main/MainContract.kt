package com.example.googlenews.core.main

import com.example.googlenews.core.base.BaseContract
import com.example.googlenews.models.ArticlesItem

object MainContract {

    interface View : BaseContract.FragmentView {
        fun hideProgressBar()
        fun showProgressBar()
        fun setNews(list: List<ArticlesItem>?)
        fun showDetailsFragment(news: ArticlesItem)
    }

    interface Presenter : BaseContract.FragmentPresenter<View> {
        fun onStartView()
        fun onSwipeToRefresh()
        fun onItemClick(news: ArticlesItem)
    }

    interface Repository {
        fun loadNews()

        interface Callback {
            fun onFetchNewsSuccess(articles: List<ArticlesItem>?)
            fun onFetchNewsFail()
        }
    }
}