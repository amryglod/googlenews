package com.example.googlenews.core.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.googlenews.R
import com.example.googlenews.models.ArticlesItem
import kotlinx.android.extensions.LayoutContainer

class MainAdapter(private val click: (item: ArticlesItem) -> Unit = {}) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var list: List<ArticlesItem>? = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_news, parent, false)
        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        list!![position].let {
            (holder as ViewHolder).bind(list!![position])
        }
    }

    override fun getItemCount(): Int {
        return list?.size ?: 0
    }

    fun setData(list: List<ArticlesItem>?) {
        this.list = list
        notifyDataSetChanged()
    }

    inner class ViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {

        init {
            containerView.setOnClickListener { click.invoke(list!![adapterPosition]) }
        }

        fun bind(model: ArticlesItem) {
            val tvName = containerView.findViewById(R.id.tvTitle) as TextView
            tvName.text = model.title

            val ivPhoto = containerView.findViewById(R.id.ivPhoto) as ImageView
            Glide.with(containerView.context)
                .load(model.urlToImage)
                .into(ivPhoto)
        }
    }
}