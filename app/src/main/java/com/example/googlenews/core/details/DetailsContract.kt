package com.example.googlenews.core.details

import com.example.googlenews.core.base.BaseContract
import com.example.googlenews.models.ArticlesItem

object DetailsContract {

    interface View : BaseContract.FragmentView {
        fun setDetailsContent(news: ArticlesItem?)
        fun getArg(): ArticlesItem
    }

    interface Presenter : BaseContract.FragmentPresenter<View> {
        fun onStartView()
    }
}