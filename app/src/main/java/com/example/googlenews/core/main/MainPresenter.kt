package com.example.googlenews.core.main

import com.example.googlenews.core.base.BaseFragmentPresenter
import com.example.googlenews.models.ArticlesItem

class MainPresenter :
    BaseFragmentPresenter<MainContract.View>(),
    MainContract.Presenter,
    MainContract.Repository.Callback {

    private var repository: MainContract.Repository? = null

    init {
        this.repository = MainRepository(this)
    }

    override fun onStartView() {
        view?.showProgressBar()
        repository?.loadNews()
    }

    override fun onItemClick(news: ArticlesItem) {
        view?.showDetailsFragment(news)
    }

    override fun onFetchNewsSuccess(articles: List<ArticlesItem>?) {
        view?.hideProgressBar()
        view?.setNews(articles)
    }

    override fun onFetchNewsFail() {
        view?.hideProgressBar()
        view?.showInternetConnectionError()
    }

    override fun onSwipeToRefresh() {
        repository?.loadNews()
    }
}
