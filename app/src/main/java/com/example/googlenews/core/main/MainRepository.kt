package com.example.googlenews.core.main

import com.example.googlenews.AppApplication
import com.example.googlenews.database.AppDatabase
import com.example.googlenews.database.ArticlesDao
import com.example.googlenews.models.ArticlesItem
import com.example.googlenews.network.ApiManager
import com.example.googlenews.network.Results
import com.example.googlenews.utils.go
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class MainRepository(callback: MainContract.Repository.Callback) :
    MainContract.Repository {

    private val uiScope = CoroutineScope(Dispatchers.Main)
    private var callback: MainContract.Repository.Callback? = null
    private var database: AppDatabase? = null
    private var articleDao: ArticlesDao? = null

    init {
        this.callback = callback
        this.database = AppDatabase.invoke(AppApplication.applicationContext())
        this.articleDao = database?.articlesDao()
    }

    override fun loadNews() {
        fetchArticlesFromApi()
    }

    private fun fetchArticlesFromApi() = uiScope.launch {
        when (val results = ApiManager.call().getNews().go()) {
            is Results.Success -> {
                removeArticlesFromDB()
                insertArticlesInDB(results.response.articles)
                callback?.onFetchNewsSuccess(results.response.articles)
            }
            is Results.NetworkError -> {
                fetchArticlesFromDB()
                callback?.onFetchNewsFail()
            }
        }
    }

    private fun fetchArticlesFromDB() = uiScope.launch {
        val articles = articleDao?.getArticles()
        callback?.onFetchNewsSuccess(articles)
    }

    private fun insertArticlesInDB(articles: List<ArticlesItem>?) = uiScope.launch {
        articles?.let {
            articleDao?.saveArticles(it)
        }
    }

    private fun removeArticlesFromDB() = uiScope.launch {
        articleDao?.deleteArticles()
    }
}