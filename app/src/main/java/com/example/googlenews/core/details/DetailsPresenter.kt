package com.example.googlenews.core.details

import com.example.googlenews.core.base.BaseFragmentPresenter

class DetailsPresenter :
    BaseFragmentPresenter<DetailsContract.View>(),
    DetailsContract.Presenter {

    override fun onStartView() {
        val news = view?.getArg()
        view?.setDetailsContent(news)
    }
}
