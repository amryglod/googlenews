package com.example.googlenews.core.base

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.googlenews.R
import com.example.googlenews.utils.toast


abstract class BaseFragment<in V : BaseContract.FragmentView, T : BaseContract.FragmentPresenter<V>> :
    Fragment(),
    BaseContract.FragmentView {

    protected abstract var presenter: T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.attachView(this as V)
    }

    override fun showError(error: String?) {
        context.toast(error ?: "")
    }

    override fun showInternetConnectionError() {
        val error = activity?.resources?.getString(R.string.error_internet_connection) ?: ""
        context.toast(error)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
}