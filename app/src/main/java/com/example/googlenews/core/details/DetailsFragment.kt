package com.example.googlenews.core.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.googlenews.R
import com.example.googlenews.core.base.BaseFragment
import com.example.googlenews.models.ArticlesItem
import com.example.googlenews.utils.changeDateFormat
import kotlinx.android.synthetic.main.fragment_details.*

class DetailsFragment : BaseFragment<DetailsContract.View, DetailsContract.Presenter>(),
    DetailsContract.View {
    override var presenter: DetailsContract.Presenter = DetailsPresenter()

    companion object {
        const val KEY_NEWS = "KeyNews"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter.onStartView()
    }

    override fun setDetailsContent(news: ArticlesItem?) {
        news?.let {
            tvTitle.text = it.title
            tvDetails.text = it.description
            tvAuthor.text = it.author
            tvDate.text = it.publishedAt?.changeDateFormat()
            Glide.with(context!!)
                .load(it.urlToImage)
                .into(ivPhoto)
        }
    }

    override fun getArg() = arguments?.getSerializable(KEY_NEWS) as ArticlesItem
}