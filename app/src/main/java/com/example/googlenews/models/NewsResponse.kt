package com.example.googlenews.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

data class NewsResponse(
    val totalResults: Int? = null,
    val articles: List<ArticlesItem>? = null,
    val status: String? = null
) : Serializable

@Entity(tableName = "Articles")
data class ArticlesItem(
    @PrimaryKey(autoGenerate = true)
    var id: Int,

    val publishedAt: String? = null,
    val author: String? = null,
    val urlToImage: String? = null,
    val description: String? = null,
    val title: String? = null,
    val url: String? = null,
    val content: String? = null
) : Serializable


