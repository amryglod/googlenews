package com.example.googlenews

import android.os.Bundle
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation

class MainActivity : AppCompatActivity() {

    private val navController: NavController by lazy {
        Navigation.findNavController(this, R.id.fragmentHost)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestWindowFeature(Window.FEATURE_NO_TITLE)
        setTheme(R.style.AppTheme)
        setContentView(R.layout.activity_main)
        initMainGraph()
    }

    private fun initMainGraph() = navController.apply {
        graph = navInflater.inflate(R.navigation.main_graph).apply {
            startDestination = R.id.fragListNews
        }
    }
}
