package com.example.googlenews.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.googlenews.models.ArticlesItem
import com.example.googlenews.utils.Constants

@Database(entities = [ArticlesItem::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun articlesDao(): ArticlesDao

    companion object {
        @Volatile
        private var instance: AppDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context,
            AppDatabase::class.java, Constants.DB_NAME
        )
            .build()
    }
}
