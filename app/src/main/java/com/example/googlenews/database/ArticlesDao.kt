package com.example.googlenews.database


import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.googlenews.models.ArticlesItem

@Dao
interface ArticlesDao {

    @Query("SELECT * FROM Articles")
    suspend fun getArticles(): List<ArticlesItem>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveArticles(articlesItem: List<ArticlesItem>)

    @Query("DELETE FROM Articles")
    suspend fun deleteArticles()
}
